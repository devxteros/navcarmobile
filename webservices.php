<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
date_default_timezone_set('America/Bogota');

function getQueryCategorias(){	
	$q = "SELECT * FROM categorias";
	return $q;
}

function getQueryProductos($fec_ini, $fec_fin){	
	$q = "SELECT 
				 referencia,
				 marca id_marca,
				 marcas.nombre marca,
				 descripcion,
				 aplicacion id_aplicacion,
				 categorias.name aplicacion, 
				 oem, 
				 armado, 
				 longitud, 
				 imagen, 
				 productos.status 
		 FROM productos, marcas, categorias 
		 WHERE productos.marca = marcas.id AND productos.aplicacion = categorias.id AND DATE(productos.updateAt) >= DATE('$fec_ini') AND DATE(productos.updateAt) <= DATE('$fec_fin')";
	
	return $q;
}

function getMarcas(){
	$q = "SELECT marcas_categorias.id,categorias.id,categorias.name, marcas.id,marcas.nombre, marcas_categorias.status from categorias, marcas, marcas_categorias WHERE marcas_categorias.id_aplicacion = categorias.id AND marcas_categorias.id_marca = marcas.id";	
	return $q;
}


$fechaInicio = (!isset($_GET['q']) || empty($_GET['q'])) ? '2017-01-01' : date('Y-m-d',strtotime($_GET['q']));
$fechaFin = date('Y-m-d');
$con=mysqli_connect("localhost","navcaruser","navcarpwd","navcar");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }


$categorias= array();
$marcas = array();
$productos = array();

//Consulta Categorias
$sql=getQueryCategorias();
$result=mysqli_query($con,$sql);

while ($row = $result->fetch_array(MYSQLI_NUM)) {    
    array_push($categorias, $row);
}

//Consulta Marcas
$sql=getMarcas();
$result=mysqli_query($con,$sql);

while ($row = $result->fetch_array(MYSQLI_NUM)) {    
    array_push($marcas, $row);
}


//Consulta Productos
$sql=getQueryProductos($fechaInicio, $fechaFin);
$result=mysqli_query($con,$sql);

while ($row = $result->fetch_array(MYSQLI_NUM)) {    
    array_push($productos, $row);
}

// Free result set
mysqli_free_result($result);
mysqli_close($con);

$response = array("result"=>["success"=>true,"categorias"=>$categorias, "marcas"=>$marcas, "items"=>$productos]);
echo json_encode($response);
?>