import React from 'react';
import { AsyncStorage } from 'react-native';
import { CONFIG } from '../config/config';
import realm from '../models/models';


export const getCatalogo = (lastDate:string) => {
  return fetch(CONFIG.APIURL+"?q="+lastDate, {
        method: 'GET'
  });
};

export const getFechaSync = async () => {
  try {    
    const value = await AsyncStorage.getItem('SYNCDATE');
    return value;
  } catch (error) {
      alert(error);
  }
}

export const setFechaSync = async (timestamp:string) => {
  try {
      await AsyncStorage.setItem('SYNCDATE',timestamp);     
  } catch (error) {
      alert(error);
  }
}

export const updateUI = (ui,value)=>{
   ui.setState({progress: value});
}

export const setCatalogo = async (vista,categorias, marcas, items) => {
	try {
    //Categorias
    if(categorias.length > 0){      
       categorias.map((categoria,index)=>{
                  realm.write(() => {
                    realm.create('Categoria', { id:categoria[0], name:categoria[1], status:categoria[2] }, true);
                  });
      });     
       
    }

    //Marcas
    if(marcas.length > 0){
      marcas.map((marca)=>{
          realm.write(() => {
                    realm.create('Marca', { id_pkey:marca[0], id_categoria:marca[1], nombre_categoria:marca[2], id:marca[3], name:marca[4], status:marca[5] }, true);
          });
      })
    }    
    
    //Productos    
    if(items.length > 0){
      items.map((item,index)=>{                  
                  realm.write(() => {                  
                    realm.create('Item',
                      {
                        referencia:item[0],
                        id_marca:item[1],
                        marca:item[2],
                        descripcion:item[3],
                        id_aplicacion:item[4],
                        aplicacion:item[5],
                        oem:item[6],
                        armado:item[7],
                        longitud:item[8],
                        imagen:item[9],
                        status:item[10]
                      },true
                    );
                  });
      });
    }
    
    } catch (error) {
      alert('setCatalogo error: ' + error.message);
    }	
}

export const getCatalogoLocal = async (storageKey) => {
	try {
	  const value = await AsyncStorage.getItem(storageKey);
	  return value;
	  /*if (value !== null){
	    return value;
	  }*/
	} catch (error) {
	  	alert(error);
	}

}