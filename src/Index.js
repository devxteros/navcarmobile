import React from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { headerToolbar } from './Toolbar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLOR } from './config/colors';
import { MESSAGES } from './config/messages';


import ContactView from './ContactView';
import TabUno from './TabUno';
import TabDos from './TabDos';
import TabTres from './TabTres';
import DetailItemView from './DetailItemView';
import SyncView from './SyncView';
import ZoomImageView from './ZoomImageView';
import RealmDB from './RealmDB';


const MainScreenNavigator = TabNavigator({
  Inicio: { screen: TabUno, navigationOptions:{title:MESSAGES.HOME, }},
  Categoria: { screen: TabDos, navigationOptions:{title:MESSAGES.CATEGORY, }},  
  Busqueda: { screen: TabTres, navigationOptions:{title:MESSAGES.SEARCH, }},  
},{
  tabBarOptions:{
    style: {backgroundColor: COLOR.PRIMARY_DARK, padding:0}, // color de fondo en las pestañas    
    upperCaseLabel:false, // coloca los textos de las pestañas en mayusculas
    showIcon: false, // mostrar icono en la pestaña
    showLabel: true, // mostrar label en la pestaña
    scrollEnabled: false, // habilita scroll horizontal en las pestañas
    pressOpacity:0.5, // opacidad del label en la pestaña activa al tocarla
    activeTintColor: COLOR.TEXT_PRIMARY, // color del label cuando la pestaña esta activa
    activeBackgroundColor: COLOR.PRIMARY,
    indicatorStyle:{backgroundColor:COLOR.SECONDARY},
  },
  initialRouteName:'Inicio', // pestaña de inicio
  lazy:true, // Indica si la carga de las pestañas es perezosa
  swipeEnabled:true, // Si permite pasar entre las pestañas
  tabBarPosition:'top', // posicion de las pestañas (top : bottom)
  backBehavior: 'none' // En caso de que el botón de retroceso cause un cambio de pestaña a la pestaña inicial  
}
);
// navigationOptions: ({ navigation }) => ({ header:headerToolbar(navigation) })
// navigationOptions:{header:headerToolbar()}
export const Index = StackNavigator({
  Home: { screen: MainScreenNavigator, navigationOptions:({ navigation }) => {  
    return {
      //title: 'Home Title', // titulo de la aplicacion
      header: headerToolbar(navigation), //Componente de la barra de navegación
    }
  } },
  Contact: { screen: ContactView },
  ItemDetail: { screen: DetailItemView, 
    navigationOptions: ({ navigation }) => {
      return {title: `${navigation.state.params.item.referencia}`}
    }
  },
  ZoomImage: { screen: ZoomImageView, navigationOptions:{header:null} },
  Sync: { screen: RealmDB, navigationOptions:{header:null} }
},{
  navigationOptions:{
    headerTintColor: COLOR.TEXT_PRIMARY, //Color del texto en la barra de titulo
    headerStyle: {backgroundColor: COLOR.PRIMARY_DARK},
    //headerLeft: headerLeft(), // Boton de navegacion izquierdo
    //headerRight: headerRight(), // Boton de navegacion derecho
    //headerTitle: MESSAGES.NAMEAPP //titulo de la barra de navegacion principal: 'null' para delegar esta funcion a las pestañas
  },
  initialRouteName:'Home', //ruta de inicio al cargar la aplicación
  headerMode: 'screen' // modo en que se muestra la barra de navegación
});
