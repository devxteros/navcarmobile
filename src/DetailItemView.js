import React, { Component } from 'react';
import { View, Image, ScrollView, Text, TouchableHighlight } from 'react-native';
import { styles } from './styles';

import ImageViewer from 'react-native-image-zoom-viewer';

export default class DetailItemView extends Component{

	constructor(){
		super()
	}

	render(){
		
		let { params } = this.props.navigation.state;
		
		return(

		<View style={{flex:1}}>
			<ScrollView>
			<TouchableHighlight style={{padding:0}} onPress={() => this.props.navigation.navigate('ZoomImage',{'image':(params.item.imagen == '')?'http://via.placeholder.com/300x200' : params.item.imagen})}>
			<Image
	          style={styles.imageDetail}
	          resizeMode={'cover'}
	          source={{uri: (params.item.imagen == '')?'http://via.placeholder.com/300x200' : params.item.imagen}} />
	         </TouchableHighlight>
	        
	        <Text style={styles.textItemTitle}>APLICACIÓN</Text>
	        <Text style={styles.textItem}>{params.item.aplicacion}</Text>
	        <Text style={styles.textItemTitle}>DESCRIPCIÓN</Text>
	        <Text style={styles.textItem}>{params.item.descripcion}</Text>
	        <Text style={styles.textItemTitle}>OEM</Text>
	        <Text style={styles.textItem}>{params.item.oem}</Text>
	        <Text style={styles.textItemTitle}>ARMADO</Text>
	        <Text style={styles.textItem}>{params.item.armado}</Text>
	        <Text style={styles.textItemTitle}>LONGITUD</Text>
	        <Text style={styles.textItem}>{params.item.longitud}</Text>
	        
	        </ScrollView>	        
		</View>

		)
		
	}

}