import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Button, ListView, AppState} from 'react-native';
import { MESSAGES } from './config/messages';
import { styles } from './styles';
import ItemList from './components/ItemList';
import { getCatalogo, getCatalogoLocal } from './actions/services';
import Spinner from './components/Spinner';
import { COLOR } from './config/colors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import realm from './models/models';



const categoryBrand = (v,text,value) => {
		return(
			<TouchableOpacity style={{flex:1}} onPress={() => v._renderListaSeleccion(value)}>		
					<View style={styles.itemCategoryBox} >      				
			      				<Text style={styles.itemCategoryTxt}>{text}</Text>
			      				<Icon style={styles.itemCategoryIco}name="chevron-right" size={26} color={COLOR.TEXT_SECONDARY} />
			      	</View>
	      	</TouchableOpacity>			
		)
}


export default class TabDos extends Component {

	static navigationOptions = ({ navigation }) => {
	  return {
	    header: (
	      <Button
	        title={MESSAGES.CONTACT}
	        onPress={() => navigation.navigate('Contact')}
	      />
	    ),
	  };
	};
	
	
	constructor(props){
		super(props);
    	let c = realm.objects('Categoria');
    	let emptyData = (c.length==0) ? true : false;
		const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = {
					loading:true,
			        emptyData:emptyData,
			        message:MESSAGES.LOADING,
			        isAplicacion:true,
			        isMarca:false,
			        isReferencia:false,		      	
					dataSource: ds,
					items:c,
					tipoConsulta:'APPLICATION',
					id_categoria:'',
					id_marca:'',
				}
	}

	updateDataSource = (data) => {
	    return this.state.dataSource.cloneWithRows(data);    
  	}


  	_renderListaSeleccion(id){

  		if(this.state.tipoConsulta == "BRAND"){
  			let p = realm.objects('Item').filtered('id_marca = "'+id+'" AND id_aplicacion = "'+this.state.id_categoria+'"');
  			let emptyData = (p.length==0) ? true : false;  			
  			this.setState({tipoConsulta:'ITEM', isReferencia:true, items:p,id_marca:id,emptyData:emptyData});

  		}else if(this.state.tipoConsulta == "APPLICATION"){
  			this.setState({tipoConsulta:"BRAND"});
  			this.setState({id_categoria:id})
  			this._renderListaMarcas(id);  			
  		}
  		
  	}

  	_renderListaMarcas(categoria){
  		let c = realm.objects('Marca').filtered('id_categoria = "'+categoria+'"');
  		let emptyData = (c.length==0) ? true : false;
  		this.setState({items:c});
  		this.setState({isAplicacion:true});
  		this.setState({isMarca:true});
	   	this.setState({isReferencia:false});
	   	this.setState({emptyData:emptyData});
  	} 	


	render(){

		return(
			<View style={{flex:1, backgroundColor:'#ECF2CE'}}>
				<View style={{flex:1, flexDirection:'row', justifyContent: 'space-around'}}>				
				{this._renderLinkAplicacion()}				
				{this._renderLinkMarca()}					
				{this._renderLinkReferencia()}				
				</View>				
				{this._renderEmptyData()}
				<View style={{flex:10, marginTop:10}}>
				{this._renderViewAplicacion()}
				{this._renderViewItems()}
				</View>
			</View>	
		)
	}


	componentWillMount (){
		this.setState({loading:true});		
	}

	componentDidMount(){
		this.setState({loading:false});		
  	}

  	
  	onLinkAplicacion(){  			   		
   		this.setState({tipoConsulta:"APPLICATION"});
		let c = realm.objects('Categoria');
		let emptyData = (c.length==0) ? true : false;
  		this.setState({items:c});
  		this.setState({isAplicacion:true});
  		this.setState({isMarca:false});
	   	this.setState({isReferencia:false});
	   	this.setState({isReferencia:false});
	   	this.setState({emptyData:emptyData});
	   	
	}

	onLinkMarca(){		
	   		this.setState({isReferencia:false});
	   		if(this.state.id_categoria != ''){
	   			this.setState({tipoConsulta:"BRAND"});
	   			this._renderListaMarcas(this.state.id_categoria);
	   		}
	}

	onLinkReferencia(){
	   		//alert('onLinkReferencia');
	}

	_renderViewItems(){
		if(this.state.tipoConsulta=='ITEM'){
			return(				
				<ItemList items={this.state.items} navigator={this.props.navigation} />
			)
		}else{
			return null;
		}
	}

	_renderViewAplicacion(){

		if(this.state.tipoConsulta=='APPLICATION' || this.state.tipoConsulta=='BRAND'){
			return(
				<View style={{flex:1, backgroundColor:'#ECF2CE'}}>				
					<ListView
						enableEmptySections={true}
			        	dataSource={this.updateDataSource(this.state.items)}
			        	renderRow={(item) => categoryBrand(this, item.name, item.id)}/>				
				</View>
			)	
		}else{
			return null;
		}
		
	}

	_renderEmptyData(){
		if(this.state.emptyData){
			return(
				<View style={{flex:1}}>		
					<Text style={styles.labelText}>{MESSAGES.EMPTY_CATALOG}</Text>
				</View>
			)
		}else{
			return null;
		}
	}


	_renderLinkAplicacion () {
		
			if(this.state.isAplicacion){
				return (
					<TouchableOpacity style={{marginTop: 5, height: 39, padding:0}} onPress={this.onLinkAplicacion.bind(this)}>
						<Text style={styles.linkFiltroCategorias}>{MESSAGES.APPLICATION}</Text>
					</TouchableOpacity>
				);
			}else{
				return null;
			}		
	}
	_renderLinkMarca() {
		
			if(this.state.isMarca){
				return(
					<TouchableOpacity style={{marginTop: 5, height: 39, padding:0}} onPress={this.onLinkMarca.bind(this)}>
						<Text style={styles.linkFiltroCategorias}>{MESSAGES.BRAND}</Text>
					</TouchableOpacity>
				);
			}else{
				return null;
			}		
	}
	_renderLinkReferencia() {
		
			if(this.state.isReferencia){
				return(
					<TouchableOpacity style={{marginTop: 5, height: 39, padding:0}} onPress={this.onLinkReferencia.bind(this)}>
						<Text style={styles.linkFiltroCategorias}>{MESSAGES.REFERENCE}</Text>
					</TouchableOpacity>	
				);
			}else{
				return null;
			}		
	}
}
