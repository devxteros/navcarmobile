import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, Dimensions, TouchableHighlight, ToolbarAndroid, Button, TextInput } from 'react-native';
import { MESSAGES } from './config/messages';
import { styles } from './styles';

export default class TabUno extends Component {

		
	static navigationOptions = ({ navigation }) => {	
	  return {
	    header: (
	      <Button
	        title={MESSAGES.CONTACT}
	        onPress={() => navigation.navigate('Contact')}
	      />
	    ),
	  };
	};
	
	
	constructor(){
		super();
		this.state = {
			code:'',
			vehicle:''			
		}
	}


	render(){

		const { navigate } = this.props.navigation;

		return(
					
			<View style={{flex:1, backgroundColor:'#FFFFFF'}}>
				<View style={{flex:1}}>
					<Image source={require('./assets/logo.png')} style={styles.imageLogo} resizeMode={'cover'} />
				</View>
				<View style={{flex:1}}>
									
					<TouchableHighlight style={{flex:1, margin:0}} onPress={() => navigate('Busqueda', {valor:this.state.code.toUpperCase(),filterType:'CODE'})}>
						<View style={styles.btnBigPrimary}>
								<Text style={styles.btnBigText}>{MESSAGES.SEARCH_BY_GENERAL}</Text>					
						</View>
					</TouchableHighlight>
					
					<TouchableHighlight style={{flex:1, margin:0}} onPress={() => navigate('Categoria', {valor:this.state.vehicle.toUpperCase(),filterType:'VEHICLE'})}>	
						<View style={styles.btnBigSecondary}>					
								<Text style={styles.btnBigText}>{MESSAGES.SEARCH_BY_CATEGORY}</Text>					
						</View>
					</TouchableHighlight>
				</View>

			</View>	
		)
	}
}
