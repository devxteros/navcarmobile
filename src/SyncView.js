import React, { Component } from 'react';
import { View, Text, ProgressBarAndroid, Image, ActivityIndicator, Button} from 'react-native';
import { MESSAGES } from './config/messages';
import { COLOR } from './config/colors';
import { styles } from './styles';
import { getCatalogo, setCatalogo, getCatalogoLocal } from './actions/services';
import Spinner from './components/Spinner';


export default class SyncView extends Component{

	state = {
        loading:true,
        message: MESSAGES.DOWNLOAD_DATA,
        items:[]
  	};

	constructor(props){
		super(props);
	}

	componentDidMount(){

		getCatalogo()
	          .then((response) => response.json())
	          .then((responseJson) => {
	              if(responseJson.result.success == true){

					  this.setState({loading: false});
					  this.setState({message: MESSAGES.STORAGE_DATA});
					  
					  setCatalogo('CATALOGO', JSON.stringify(responseJson.result.items))
					  .then( () => {					  	
					  	this.setState({message: MESSAGES.STORAGE_DATA_OK});
					  });

	              }else{
	              	  this.setState({loading: false});
	                  this.setState({message: MESSAGES.DOWNLOAD_DATA_FAIL});
	              }
	          })
	          .catch((error) => {
	            alert(error);
	    });

	}

	render(){

		return(
			<View style={{flex:1}}>
				<View style={styles.sync}>
					<Image source={require('./assets/logosmall.png')} />
					<Text style={styles.btnBigText}>{this.state.message}</Text>
					{(this.state.loading == true) ?
						<ActivityIndicator 
	                        color={COLOR.SECONDARY}
	                        size={"large"}
	                        animating={true}       
	                    />
                    	:
                    	null  
                	}

                	{(this.state.loading == false) ?
	                	<View style={{marginTop:20}}>
		                	<Button
					          onPress={() => this.props.navigation.goBack(null)}
					          title={MESSAGES.GOBACK}
					          color={COLOR.SECONDARY}
					        />
				        </View>
			        :
			        	null
			    	}
				</View>
			</View>
		)

	}


}