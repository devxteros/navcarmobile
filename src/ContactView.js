import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, Image, TextInput, ScrollView } from 'react-native';
import Spinner from './components/Spinner';
import NavigationBar  from './components/NavigationBar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { MESSAGES } from './config/messages';
import { CONFIG } from './config/config';
import styles from './styles';
import { COLOR } from './config/colors';

export default class ContactView extends Component {

  static navigationOptions = {
    title: MESSAGES.CONTACT,
  }

	constructor() {
	    super();
	    this.state = {
	      loading: false,
        labelBtnSend: MESSAGES.SEND,
        name: '',
        email: '',
        phone:'',
        comment: ''
	    };
  	}

  render() {
    return (
    	<View style={{ flex: 1 }}>    		        			  
        <ScrollView>
          <Text style={{marginTop:20, marginLeft: 12, fontSize:16}}>
            {MESSAGES.CONTACTINFO}
          </Text>

          <TextInput
            ref="1"
            placeholder={MESSAGES.NAME}
            onChangeText={(name) => this.setState({name})}
            returnKeyType='next'
            onSubmitEditing={() => this.focusNextField('2')}
          />

          <TextInput
            ref="2"
            placeholder={MESSAGES.EMAIL}
            onChangeText={(email) => this.setState({email})}
            keyboardType='email-address'
            returnKeyType='next'
            onSubmitEditing={() => this.focusNextField('3')}
          />

          <TextInput
            ref="3"          
            placeholder={MESSAGES.PHONE}
            onChangeText={(phone) => this.setState({phone})}
            keyboardType='phone-pad'
            returnKeyType='next'
            onSubmitEditing={() => this.focusNextField('4')}
          />

          <TextInput
                  ref="4" 
                  placeholder={MESSAGES.COMMENTS}
                  keyboardType='default'
                  multiline={true}
                  numberOfLines={9}
                  onChangeText={(comment) => this.setState({comment})}                  
          />

          
          <Button
            onPress={this.onSubmit.bind(this)}
            title={this.state.labelBtnSend}
            color={COLOR.PRIMARY_DARK}            
          />
        </ScrollView>        			
      </View>
    )
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onSubmit(){
    
    const queryStringArr = [];    
    const parametros = {
      tk: CONFIG.MAILGUN.APIKEY,      
      nom: this.state.name,
      mail: this.state.email,
      tel: this.state.phone, 
      coment: this.state.comment
    }

      for (let p in parametros) {
        queryStringArr.push(encodeURIComponent(p) + "=" + encodeURIComponent(parametros[p]));
      }
      const body = queryStringArr.join("&");    

      this.setState({'labelBtnSend' : MESSAGES.SEND_CONTACT});

      fetch(CONFIG.MAILGUN.API_BASE_URL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: body
      })      
      .then((responseJson) => {
        //console.warn(responseJson.message);
        this.setState({'labelBtnSend' : MESSAGES.SEND_CONTACT_OK});        
      })
      .catch((error) => {
        console.error(error);
      });
  }

}