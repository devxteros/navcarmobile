import { StyleSheet, Dimensions } from 'react-native';
import { COLOR } from './config/colors';

var {height, width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  container:{
    flex:1
  },
  labelText: {
  	fontSize: 17,
  	color: '#333333',
  	letterSpacing: 0.5
  },
  icon: {
    width: 24,
    height: 24,
  },
  contactContainer:{
  	padding: 16,  	
  },
  inputMultiLine:{  	
    fontSize: 16,
    textAlignVertical: 'top'
  },
  containerButton:{
      flex:1,
      justifyContent: 'center',      
  },
  title: {
    fontSize: 18,
    color: COLOR.TEXT_PRIMARY
  },
  navigationBar:{
    flex:0.3,
    backgroundColor: COLOR.PRIMARY_DARK,
    alignItems: 'center',
    justifyContent: 'center',     
  },
  btnBigText:{
    color:COLOR.TEXT_PRIMARY,
    fontWeight:'bold',
    textAlign:'center',
  },
  btnBigPrimary:{    
    flex:1, 
    alignItems:'center',
    justifyContent:'center', 
    backgroundColor:COLOR.PRIMARY,
    borderRadius:0,
  },
  btnBigSecondary:{    
    flex:1, 
    alignItems:'center',
    justifyContent:'center', 
    backgroundColor:COLOR.SECONDARY,
  },
  buscarCodigo:{
    color: COLOR.TEXT_PRIMARY,
    backgroundColor: COLOR.SECONDARY,
    height:80,
    textAlign:'center',
    fontWeight:'bold'
  },
  imageLogo:{
    flex:1,
    height:null,
    width:null
  },
  containerSearch:{
    flex:1,
    alignItems:'center'
  },
  msgSearch:{
    fontWeight:'bold',
    fontSize:15,
    textAlign:'center'
  },
  textTip:{
    textAlign:'center',
    width:300,
    fontSize:16,
  },
  imageDetail:{    
    width,
    height:200
  },
  textItemTitle:{
    fontSize:16,
    fontWeight:'bold',
    marginLeft: 10,
    marginTop: 10
  },
  textItem:{
    fontSize:14,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 0
  },
  sync:{
    flex:1,
    backgroundColor: COLOR.PRIMARY_DARK,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemCategoryBox:{
    flex:1,
    flexDirection:'row', 
    justifyContent: 'space-between',
    height:50,    
  },
  itemCategoryTxt:{
    flex:1,
    fontSize:13,
    marginLeft:5,
    borderBottomWidth:1
  },
  itemCategoryIco:{
    flex:0.1,
    borderBottomWidth:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor:COLOR.SECONDARY
  },
  linkFiltroCategorias:{
    color: COLOR.TEXT_SECONDARY, 
    fontSize:16,
    textAlign:'center',    
    borderBottomColor: COLOR.PRIMARY_DARK,
    //borderBottomWidth: 1,
    backgroundColor:COLOR.PRIMARY_LIGHT,
    padding:5

  },
  labelText: {
      fontSize: 18,
      color: COLOR.PRIMARY_DARK,
      textAlign:'center'
  },
  picker:{
    color:'#000',
    borderBottomWidth: 2,    
    borderColor:'#000',
    backgroundColor:'#DDD'
  }
})