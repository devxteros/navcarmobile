import React, { Component } from 'react';
import { View, Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { NavigationActions } from 'react-navigation';


export default class ZoomImageView extends Component{

	constructor(){
		super()
	}

	onBack(){
		alert('volvemos atras...');		
	}

	render(){
				
		let image = [{url: this.props.navigation.state.params.image}];

		return(
		<View style={{flex:1}}>
			<ImageViewer imageUrls={image}/>	        
		</View>

		)		
	}
}