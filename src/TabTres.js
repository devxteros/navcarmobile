import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Button, Picker, ListView, TextInput, Alert} from 'react-native';
import { MESSAGES } from './config/messages';
import { styles } from './styles';
import Spinner from './components/Spinner';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLOR } from './config/colors';
import ItemList from './components/ItemList';
import realm from './models/models';


export default class TabTres extends Component {
	
	static navigationOptions = ({ navigation }) => {	
	  return {	    
	    header: (
	      <Button
	        title={MESSAGES.CONTACT}
	        onPress={() => navigation.navigate('Contact')}
	      />
	    ),
	  };
	};

	state = {
        loading:true,
        select:'',
        text:'',
        items:[]
  	};
	
	
	constructor(props){
		super(props);		
	}


	render(){
		return(
			<View style={{flex:1, backgroundColor:'#ECF2CE'}}>				
				<View style={{flex:1, paddingLeft:2, paddingRight:2}}>
					<Picker
					  style={styles.picker}
					  selectedValue={this.state.select}
					  onValueChange={(itemValue, itemIndex) => this.setState({select:itemValue})}
					  >
					  <Picker.Item label={MESSAGES.SELECT_CRITERIA} value="" />
					  <Picker.Item label={MESSAGES.APPLICATION} value="APLICACION" />
					  <Picker.Item label={MESSAGES.BRAND} value="MARCA" />
					  <Picker.Item label={MESSAGES.REFERENCE} value="REFERENCIA" />
					</Picker>
					<TextInput
				        style={{height: 40, backgroundColor:'#EEE'}}
				        onChangeText={(text) => this.setState({text})}				        
				        maxLength={50}
				        placeholder={MESSAGES.SEARCH_CODE}
				        keyboardType={'default'}
				        returnKeyType={'done'}
				        //onEndEditing={(e) => this._searchByCriteria(this.state.select, e.nativeEvent.text)}
				    />
					<Button
						onPress={()=>this._searchByCriteria(this.state.select, this.state.text)}
						title={MESSAGES.SEARCH}
						color={COLOR.SECONDARY}
					/>
				    <ItemList items={this.state.items} navigator={this.props.navigation} />
				</View>
			</View>	
		)
	}

	
  	_searchByCriteria(select, text){

  		let p;  		
  		let str = text.toUpperCase();
  		
  		if(select == ''){
  			Alert.alert(MESSAGES.WARN_TITLE,MESSAGES.WARN_MSG_CRITERIA_SEARCH);
  			return false;
  		}

  		this.setState({select: select});
  		this.setState({text: text});
  		

  		if(select == 'APLICACION'){
  			 p = realm.objects('Item').filtered('aplicacion CONTAINS "'+str+'"');
  		}else if(select == 'MARCA'){
  			 p = realm.objects('Item').filtered('marca CONTAINS "'+str+'"');
  		}else if(select == 'REFERENCIA'){
  			 p = realm.objects('Item').filtered('referencia CONTAINS "'+str+'"');
  		}

  		this.setState({items:p});

  		if(p.length==0){  			
  			Alert.alert(MESSAGES.WARN_TITLE,MESSAGES.WARN_EMPTY_SEARCH);
  		}

  	}
}
