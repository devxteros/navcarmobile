/*
Componente Generico Spinner
@author: Juan Manuel Lora <juanmalora@gmail.com>
@date: Abril 25 de 2017
*/
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import NavBar, { NavGroup, NavButton, NavButtonText, NavTitle } from 'react-native-nav';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLOR } from '../config/colors.js';
 
const NavigationBar = (props) => {
    
      return (
	      	<NavBar style={styles}>
		      	<TouchableOpacity onPress={()=> props.nav.navigate('DrawerOpen')}>
			      <Icon name="menu" size={28} color="#fff" />
	    		</TouchableOpacity>
		      	
		      	<NavTitle style={styles.title}>		      	
		      		{props.title}
		      	</NavTitle>
				<NavGroup>
					<NavButton style={styles.navButton} onPress={() => props.nav.navigate('Contacto')}>
				      	{props.iconsearch ? 
					      	
					      		<Icon name="magnify" style={styles.icon} size={28} />
					      	 : null
				      	}
			      	</NavButton>
		      	</NavGroup>

	      	</NavBar>            
      );    
}
 
const styles = StyleSheet.create({
	statusBar: {
    	backgroundColor: COLOR.PRIMARY_DARK,
  	},
  	title: {
    	color: COLOR.TEXT_PRIMARY,    	
  	},
    navBar: {
	    backgroundColor: COLOR.PRIMARY,	    
		borderTopWidth: 0,
	    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
	    borderBottomWidth: 1,
	    flexDirection: 'row',
	    justifyContent: 'space-between',
	    alignItems: 'center',
	},	
	icon: {
    	color: COLOR.TEXT_PRIMARY,
  	}
});

export default NavigationBar;