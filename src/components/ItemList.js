/**
 * ItemList Component
 * @author: Juan Manuel Lora <juanmalora@gmail.com>
 * @date: Abril 4 de 2017
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  ListView,
} from 'react-native';

import ItemBox from './ItemBox';

export default class ItemList extends Component {
  
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => { r1 !== r2 }});
    this.state = {
      dataSource: ds
    }
  }

  updateDataSource = (data) => {
    return this.state.dataSource.cloneWithRows(data);    
  }

  render() {
    let items = this.props.items || [];
    return (
      <ListView
        enableEmptySections={true}
        dataSource={this.updateDataSource(items)}
        renderRow={(artist) => <ItemBox artist={artist} navigator={this.props.navigator} />}
      />
    );
  }
}