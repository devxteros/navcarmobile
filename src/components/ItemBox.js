/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ItemBox extends Component {

  render() {

    const item = { referencia, marca, descripcion, aplicacion, oem, armado, longitud, imagen} = this.props.artist    

    return (
      <TouchableHighlight onPress={() => this.props.navigator.navigate('ItemDetail',{'item':item})}>
      <View style={styles.taskbox}>
        <View style={styles.iconContainer}>                    
          <Image style={{flex: 1, width:100}} resizeMode={'cover'} source={{uri: (imagen=='')?'http://via.placeholder.com/100x100':imagen}} />
        </View>
        <View style={styles.info}>          
            <View>
              <Text style={styles.date}>{aplicacion}</Text>
            </View>
            <View>
              <Text style={styles.customer}>{descripcion}</Text>
            </View>
            <View>              
              <Text style={styles.task}>{referencia}</Text>
            </View>
            <View>
              <Text style={styles.project}>{oem}</Text>
            </View>          
        </View>        
      </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  taskbox: {
    margin: 1,
    backgroundColor: 'white',
    flexDirection: 'row',    
  },
  info: {
    flex: 4,
    alignItems: 'stretch',
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 5
  },
  date: {
    fontSize: 12,    
    color: '#999',
  },
  customer: {
    fontSize: 12,    
    color: '#999',
  },
  task: {
    fontSize: 15,    
    color: '#222',
  },
  project: {
    fontSize: 13,    
    color: '#777',
  },
  iconContainer: {    
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#ddd'
  },
  hours: {
    fontSize: 15,    
    color: '#777',
    fontWeight: 'bold',    
  },  
});