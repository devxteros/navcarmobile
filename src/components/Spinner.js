/*
Componente Generico Spinner
@author: Juan Manuel Lora <juanmalora@gmail.com>
@date: Abril 25 de 2017
*/
import React, { Component } from 'react';
import { StyleSheet, Text, ActivityIndicator, Dimensions, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { COLOR } from '../config/colors.js';

//const { width, height } = Dimensions.get("window");
 
const Spinner = (props) => {

    if (props.visible) {
        return (
            
              <View style={styles.container}>                   
                      {(props.animating == true) ? 
                        <ActivityIndicator 
                          color={COLOR.PRIMARY_DARK}
                          size={"large"}
                          animating={true}       
                        />
                      :
                      null
                    }
                    <Text style={styles.labelText}> {props.text}</Text>
              </View>            
      );
    }else{
        return null
    }      
}

const styles = StyleSheet.create({
    container:{
      flex:1,      
      flexDirection: "row",
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 100
      //backgroundColor: COLOR.PRIMARY_DARK,      
    },
    labelText: {
      fontSize: 18,
      color: COLOR.PRIMARY_DARK
    },
});

export default Spinner;