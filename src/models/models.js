import Realm from 'realm';

class Categoria extends Realm.Object {}
Categoria.schema = {
    name: 'Categoria',
    primaryKey: 'id',
    properties: {
          id:'string', 
          name:'string',
          status:'string' 
    },
};

class Marca extends Realm.Object {}
Marca.schema = {
    name: 'Marca',
    primaryKey: 'id_pkey',
    properties: {
          id_pkey:'string',
          id_categoria:'string',
          nombre_categoria:'string',
          id:'string',
          name:'string',
          status:'string'
    },
};

class Item extends Realm.Object {}
Item.schema = {
    name: 'Item',
    primaryKey: 'referencia',
    properties: {
          referencia:'string', 
          id_marca:'string',
          marca:'string',          
          descripcion:'string', 
          id_aplicacion:'string',
          aplicacion:'string',           
          oem:'string', 
          armado:'string', 
          longitud:'string', 
          imagen:'string', 
          status:'string'
    },
};

export default new Realm({schema: [Categoria, Marca, Item]});